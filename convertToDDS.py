import os
from PIL import Image
import imageio

def convert_png_to_dds(input_folder, output_folder):
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    for filename in os.listdir(input_folder):
        if filename.endswith(".png"):
            png_path = os.path.join(input_folder, filename)
            dds_path = os.path.join(output_folder, os.path.splitext(filename)[0] + ".dds")

            # Open the PNG image
            image = Image.open(png_path)
            
            # Convert to RGBA if not already in that mode
            if image.mode != 'RGBA':
                image = image.convert('RGBA')
            
            # Save the image as DDS using imageio
            imageio.imwrite(dds_path, image, format='DDS')

            print(f"Converted {png_path} to {dds_path}")

if __name__ == "__main__":
    input_folder = "final_images"
    output_folder = "final_images_dds"
    convert_png_to_dds(input_folder, output_folder)
