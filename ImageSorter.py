import json
import os
import shutil

# Define a function to extract keys and tiers from the data
def extract_keys_and_tiers(data, result=None):
    if result is None:
        result = []
    
    if isinstance(data, dict):
        if 'key' in data and 'tier' in data:
            result.append((data['key'], data['tier']))
        
        if 'children' in data:
            for child in data['children']:
                extract_keys_and_tiers(child, result)
    elif isinstance(data, list):
        for item in data:
            extract_keys_and_tiers(item, result)
    
    return result

# List of file names to process
file_names = ['engineering.json', 'physics.json', 'society.json', 'anomalies.json']

# Iterate over each file and extract keys and tiers
all_keys_and_tiers = []
for file_name in file_names:
    with open(file_name, 'r') as file:
        data = json.load(file)
        keys_and_tiers = extract_keys_and_tiers(data)
        all_keys_and_tiers.extend(keys_and_tiers)

# Create a dictionary for easy lookup
keys_to_tiers = {key: tier for key, tier in all_keys_and_tiers}

# Path to the images folder
images_folder = 'technologies/imagesPNG'

# Iterate over the images in the images folder
for image_file in os.listdir(images_folder):
    if image_file.endswith('.png'):
        # Extract the key from the image filename (assuming the filename is the key)
        key = os.path.splitext(image_file)[0]
        
        if key in keys_to_tiers:
            tier = keys_to_tiers[key]
            tier_folder = f'T{tier}'  # No adjustment to tier level
            
            # Create the tier folder if it doesn't exist
            if not os.path.exists(tier_folder):
                os.makedirs(tier_folder)
            
            # Move the image to the appropriate tier folder
            shutil.copy(os.path.join(images_folder, image_file), os.path.join(tier_folder, image_file))
        else:
            tier_folder = 'remaining'  # Remaining files are needed to work and are stored separately
            
            # Create the tier folder if it doesn't exist
            if not os.path.exists(tier_folder):
                os.makedirs(tier_folder)
            
            # Move the image to the appropriate tier folder
            shutil.copy(os.path.join(images_folder, image_file), os.path.join(tier_folder, image_file))            

print("Images have been sorted into their respective tier folders.")
