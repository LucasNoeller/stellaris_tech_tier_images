import os
import shutil
from PIL import Image, ImageDraw, ImageFont

# Function to add text to image
def add_text_to_image(image_path, text, output_path):
    image = Image.open(image_path)
    draw = ImageDraw.Draw(image)
    
    ## Does not really work, since it doesnt add a border but more like some shades
    # Choose a larger font size for back ground
    font = ImageFont.truetype("arial.ttf", 24)  # Ensure arial.ttf is available or specify another .ttf file
    # Get text bounding box
    text_bbox = draw.textbbox((0, 0), text, font=font)
    text_width, text_height = text_bbox[2] - text_bbox[0], text_bbox[3] - text_bbox[1]
    # Position for the text (upper right corner)
    width, height = image.size
    position = (width - text_width - 2, 2)  # Adjust 2 for padding    
    # Add text to image
    draw.text(position, text, font=font, fill="black")
        
    # Choose a font size for actual text
    font = ImageFont.truetype("arial.ttf", 20)  # Ensure arial.ttf is available or specify another .ttf file
    # Get text bounding box
    text_bbox = draw.textbbox((0, 0), text, font=font)
    text_width, text_height = text_bbox[2] - text_bbox[0], text_bbox[3] - text_bbox[1]    
    # Position for the text (upper right corner)
    width, height = image.size
    position = (width - text_width - 3, 4)  # Adjust 2 for padding or 3, 4 for border
    # Add text to image
    draw.text(position, text, font=font, fill="white")
    
    # Save the image to the output path
    image.save(output_path)

# Base directory is the current working directory
base_dir = os.getcwd()

# Directory to save final images
final_images_dir = os.path.join(base_dir, "final_images")
os.makedirs(final_images_dir, exist_ok=True)

# Loop through folders T0 to T5
for i in range(0, 6):
    folder_name = f"T{i}"
    folder_path = os.path.join(base_dir, folder_name)
    
    # Check if folder exists
    if os.path.exists(folder_path):
        # Loop through all .png files in the folder
        for filename in os.listdir(folder_path):
            if filename.endswith(".png"):
                file_path = os.path.join(folder_path, filename)
                output_path = os.path.join(final_images_dir, f"{filename}")
                add_text_to_image(file_path, str(i), output_path)
    else:
        print(f"No folder called: T{i} found")

# copy remaining images
folder_name = "remaining"
folder_path = os.path.join(base_dir, folder_name)

# Check if folder exists
if os.path.exists(folder_path):
    # Loop through all .png files in the folder
    for filename in os.listdir(folder_path):
        if filename.endswith(".png"):
            file_path = os.path.join(folder_path, filename)
            output_path = os.path.join(final_images_dir, f"{filename}")
            shutil.copy(os.path.join(folder_name, filename), os.path.join(final_images_dir, filename))
else:
    print("No folder called: " + folder_name + " found")

print("Processing complete.")
